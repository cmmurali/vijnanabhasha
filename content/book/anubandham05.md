+++
title = "5 സ്വതന്ത്ര ലഭ്യതയിലുള്ള പുസ്തകങ്ങളും ജേണലുകളും ലഭിക്കുന്ന ചില സൈറ്റുകള്‍"
description = ""
weight = 250
draft = false
toc = true
bref = ""
+++

{{< rawhtml >}}

<p>Research articles, monographs and theses</p>
<p>1. Directory of Open Access Journals (DOAJ): (doaj.org/): </p>
<p> Nearly 10,000 journals on multiple topics; about half are searchable
at the article level.</p>
<p>2. Project Gutenberg: <a href="https://www.gutenberg.org/" target="_blank">https://www.gutenberg.org/</a></p>
<p> Provides free access to more than 33,000 (primarily older public
domain) books which can be downloaded in a variety of formats including
ePub, Kindle, HTML and plain text.</p>
<p>3. HathiTrust Digital Library (<a href="https://www.hathitrust.org/" target="_blank">https://www.hathitrust.org/</a>)</p>
<p> HathiTrust is a partnership of major research institutions and
libraries working to ensure that the cultural record is preserved and
accessible well into the future. The HathiTrust Digital Library includes
millions of digitized book titles, most of which were published before
1923.</p>
<p>4. OpenDOAR (<a href="https://v2.sherpa.ac.uk/opendoar/" target="_blank">https://v2.sherpa.ac.uk/opendoar/</a>)</p>
<p> A directory of open access repositories</p>
<p>5. Google Books (<a href="https://books.google.com/?hl=en" target="_blank">https://books.google.com/?hl=en</a>)</p>
<p> Google Books is a service from Google Inc. that searches the full
text of books and magazines that Google has scanned, converted to text
using optical character recognition, and stored in its digital
database.</p>
<p>6. Open Access Content (Firstsearch)
(<a href="https://cmich.idm.oclc.org/login?url=https://firstsearch.oclc.org/fsip&amp" target="_blank">https://cmich.idm.oclc.org/login?url=https://firstsearch.oclc.org/fsip&amp</a>;dbname=OpenAccessContent&amp;screen=advanced)</p>
<p> Content from over 1000 publishers is represented by collections of
the following open access providers: Biomed Central, Directory of Open
Access Journals, JSTOR, Open Access Publishing in European Networks,
OpenEdition, Public Library of Science</p>
<p>7. PubMed Central (PMC) (ttps://www.ncbi.nlm.nih.gov/sites/entrez?
db=pmc&amp;cmd=search&amp;term=)</p>
<p> PMC is a free full-text archive of biomedical and life sciences
journal literature at the U.S. National Institutes of Health's National
Library of Medicine (NIH/NLM).</p>
<p>8. Directory of Open Access Books (DOAB) -
(<a href="https://www.doabooks.org/" target="_blank">https://www.doabooks.org/</a>)</p>
<p> A directory for publishers of monographs to provide records of their
open access publications. </p>
<p>9. DART-Europe E-theses Portal.
(<a href="https://www.dart-europe.org/basic-search.php" target="_blank">https://www.dart-europe.org/basic-search.php</a>)</p>
<p> Dart-Europe is a partnership of European research libraries and
consortia which aims to improve access to European research theses</p>
<p>10. Open Access Theses and Dissertations (OATD) (<a href="https://oatd.org/" target="_blank">https://oatd.org/</a>)-
</p>
<p> It indexes records of theses from institutions across the world and
provides a simple search interface.</p>
<p>11. EThOS (<a href="https://ethos.bl.uk/" target="_blank">https://ethos.bl.uk/</a>)- provides a national aggregated
record of all doctoral theses awarded by UK Higher Education
institutions, and free access to the full text of as many theses as
possible.</p>
<p>Preprints, working papers and research data</p>
<p>• arXiv - (<a href="https://arxiv.org/" target="_blank">https://arxiv.org/</a>)(pronounced ‘archive’) a repository of
preprints across the sciences.</p>
<p>• bioRxiv (<a href="https://www.biorxiv.org/" target="_blank">https://www.biorxiv.org/</a>)- a preprint server for
unpublished papers in biology.</p>
<p>• DataCite (<a href="https://search.datacite.org/" target="_blank">https://search.datacite.org/</a>)- a non-profit organisation
that allows organisations to register research outputs, including
datasets and other unpublished works like working papers. DataCite
provides a search interface for the work registered in its database.</p>
<p>• SocArXiv (<a href="https://osf.io/preprints/socarxiv" target="_blank">https://osf.io/preprints/socarxiv</a>)- accepts papers from
across the social sciences and humanities after moderation.</p>
<p>• SSRN (<a href="https://www.ssrn.com/" target="_blank">https://www.ssrn.com/</a>)- originally focused on the social
sciences but now also contains work in the humanities and sciences.
Please note that unlike many preprint servers, users may be required to
register to access articles.</p>
<nav class="pagination pager flat">
    <ul>
        <li class="prev">
            <span class="label tag">Prev</span>
             <a href="../anubandham04">4 ബർലിൻ പ്രഖ്യാപനം</a>
        </li>
        <li class="next">
            <span class="label tag">Next</span>
            <a href="../ending">നന്ദി</a>
        </li>
    </ul>
</nav>
{{< /rawhtml >}}
