+++
title = "അവലംബം"
description = ""
weight = 190
draft = false
toc = true
bref = ""
+++

{{< rawhtml >}}

<ul>
<li><p>അരവിന്ദന്‍ കെ പി ഡോ., പ്രൊഫ. കെ പാപ്പുട്ടി, സി എം മുരളീധരന്‍ </p>
<p>(എഡിറ്റര്‍മാര്‍), ശാസ്ത്രവും കപടശാസ്ത്രവും, നാലാം പതിപ്പ്, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2011</p></li>
<li><p>ഇ എം എസ്, മാര്‍ക്സിസത്തിന്റെ പ്രസക്തി ഇന്ന്, </p>
<p>ചിന്ത പബ്ലിഷേഴ്സ്, തിരുവനന്തപുരം, 1983</p></li>
<li><p>ഇക്ബാല്‍ ബി, ഡി എ‍ന്‍ ‍എ ചരിത്രത്തില്‍ അവഗണിക്കപ്പെട്ടവര്‍,</p>
<p><a
href="https://www.deshabhimani.com/special/news-22-09-2019/823391">https://www.deshabhimani.com/special/news-22-09-2019/823391</a></p></li>
<li><p>ഇലിയിന്‍ എം, വൈ സെഗാല്‍, മനുഷ്യന്‍ എങ്ങനെ മഹാശക്തനായി, </p>
<p>കെ സി ജോര്‍ജ്, വെളിയം ഭാര്‍ഗവന്‍ (പരിഭാഷ) റാദുഗ പബ്ലിഷേഴ്സ്, </p>
<p>സോവിയറ്റ് യൂണിയന്‍, 1986</p></li>
<li><p>ഉമ്മര്‍കുട്ടി എ എന്‍ പി ഡോ., സമൂഹബന്ധങ്ങളുടെ ഉള്‍പൊരുള്‍, സോനാമംതാ ബുക്സ്,
തലശ്ശേരി</p></li>
<li><p>ഒരു സംഘം ലേഖകര്‍, വിദ്യാഭ്യാസ പരിവര്‍ത്തനത്തിന് ഒരാമുഖം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, കൊച്ചി‍, 2002</p></li>
<li><p>ഒരു സംഘം ലേഖകര്‍, ഗലീലിയോ, ഡാര്‍വിന്‍: മനുഷ്യചിന്തയെ മാറ്റിമറിച്ച മനീഷികള്‍,
കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2009</p></li>
<li><p>കാരെന്‍സാക്ക്സ്, എംഗല്‍സിന്റെ സിദ്ധാന്തങ്ങള്‍- ഒരു പുനര്‍വിചിന്തനം, സ്ത്രീ, ശാസ്ത്രം,
സമൂഹം, ഡോ. ടി കെ ആനന്ദി, ഡോ. കെ എന്‍ ഗണേശ് (എഡിറ്റര്‍മാര്‍), കേരള ശാസ്ത്രസാഹിത്യ
പരിഷത്ത്, തൃശൂര്‍, 2005</p></li>
<li><p>ഗണേശ് കെ എന്‍, അറിവിന്റെ സാര്‍വത്രികത, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2017</p></li>
<li><p>——— നവകേരളവും ജ്ഞാനസമൂഹവും, ശാസ്ത്രഗതി, 2021മെയ്, </p>
<p>വാല്യം 55 ലക്കം 10</p></li>
<li><p>ഗോവിന്ദപ്പിള്ള പി, വൈജ്ഞാനികവിപ്ലവം ഒരു സാംസ്കാരിക ചരിത്രം, </p>
<p>രണ്ടാം പതിപ്പ്, കേരള ഭാഷാ ഇന്‍സ്റ്റിറ്റ്യൂട്ട്, 2014</p></li>
<li><p>ഗോഡ്ബൊളെ രോഹിണി, രാം രാമസ്വാമി (എഡിറ്റര്‍മാര്‍), ലീലാവതിയുടെ പെണ്‍മക്കള്‍, കെ
രമ (പരി), കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2013</p></li>
<li><p>ചൈല്‍ഡ് ഗോര്‍ഡന്‍, മനുഷ്യന്‍ സ്വയം നിര്‍മിക്കുന്നു, പരിഭാഷ സി അച്യുതമേനോന്‍, കേരള ഭാഷാ
ഇന്‍സ്റ്റിറ്റ്യൂട്ട്, തിരുവനന്തപുരം, 1989</p></li>
<li><p>——— ചരിത്രത്തില്‍ എന്തു സംഭവിച്ചു?, രണ്ടാം പതിപ്പ്, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2012</p></li>
<li><p>ജനകീയ ശാസ്ത്രം: കാഴ്ചപ്പാടും കര്‍മപരിപാടിയും, |</p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 1988</p></li>
<li><p>ജനാര്‍ദനന്‍ കെ ആര്‍ പ്രൊഫ., ലോകത്തെവിടെയും സംഭാഷണം </p>
<p>സ്വന്തഭാഷയില്‍, അപ്രകാശിത ലേഖനം. </p></li>
<li><p>ജെ എസ് അടൂർ, ഭാഷയും മനുഷ്യരും സമൂഹവും,</p>
<p><a
href="http://bodhigram.blogspot.com/2019/09/blog-post.html">http://bodhigram.blogspot.com/2019/09/blog-post.html</a></p></li>
<li><p>ജോണ്‍ കുന്നത്ത്, അറിവിനെ അറിയാം, <a
href="http://john-kunnathu.blogspot.com/2020/07/blog-post_29.html">http://john-kunnathu.blogspot.com/2020/07/blog-post_29.html</a></p></li>
<li><p>ദാമോദരന്‍ വി കെ പ്രൊഫ, ജന്തുലോകത്തിലെ എഞ്ചിനീയര്‍മാര്‍, </p>
<p>രണ്ടാം പതിപ്പ്, കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2010</p></li>
<li><p>ദേവരാജൻ ടി കെ, ശാസ്ത്രവും മാനവികവിഷയങ്ങളും,</p>
<p><a
href="https://luca.co.in/scientific-temper-humanity-faith/">https://luca.co.in/scientific-temper-humanity-faith/</a></p></li>
<li><p>ധനമന്ത്രി ഡോ തോമസ് ഐസക്കിന്റെ ബജറ്റ് പ്രസംഗം, <a
href="https://keralataxes.gov.in/wp-content/uploads/2021/01/Budget-Speech_2021-Malayalam.pdf">https://keralataxes.gov.in/wp-content/uploads/2021/01/Budget-Speech_2021-Malayalam.pdf</a></p></li>
<li><p>നമ്പൂതിരി എ എന്‍ ഡോ., ബാര്‍ബറാ മക്‌ലിന്റോക്ക് : ആത്മവിശ്വാസത്തിന്റെ ഇതിഹാസം,
കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, കൊച്ചി, 1997</p></li>
<li><p>നാരായണന്‍ സി പി, ശാസ്ത്രബോധം നൂറ്റാണ്ടുകളിലൂടെ, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍</p></li>
<li><p>പരമേശ്വരന്‍ എം പി ഡോ., അറിവിന്റെ പൊരുള്‍,</p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2005</p></li>
<li><p>പാപ്പുട്ടി കെ പ്രൊഫ., ഇന്ത്യയുടെ ശാസ്ത്രപാരമ്പര്യം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2017</p></li>
<li><p>——— ജ്യോതിഷവും ജ്യോതിശ്ശാസ്ത്രവും, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, കൊച്ചി‍, 2002</p></li>
<li><p>പുരുഷോത്തമന്‍ പി വി, വിഗോട്സ്കിയും വിദ്യാഭ്യാസവും, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2008</p></li>
<li><p>——— പുരോഗമന വിദ്യാഭ്യാസ ചിന്തകര്‍, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2006</p></li>
<li><p>ബര്‍ണല്‍ ജി, ശാസ്ത്രം ചരിത്രത്തില്‍, വാല്യം 4, </p>
<p>സാമൂഹ്യശാസ്ത്രത്തിന്റെ ക്രോഡീകരണം, വിവ. എം സി നമ്പൂതിരിപ്പാട്, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, കൊച്ചി-2000</p></li>
<li><p>ബാലകൃഷ്ണന്‍ ചെറൂപ്പ ഡോ., ഫോസിലുകളും പരിണാമവും, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2009</p></li>
<li><p>ബ്രൈസന്‍ ബില്‍, പ്രപഞ്ചമഹാകഥ: എല്ലാ പ്രപഞ്ചവസ്തുക്കളുടെയും</p>
<p>ഹ്രസ്വചരിത്രം, മൂന്നാം പതിപ്പ്, വിവ വി ടി സന്തോഷ്കുമാര്‍, ഡി സി ബുക്സ്, കോട്ടയം,
2013</p></li>
<li><p>മധുസൂദനന്‍ പി, പ്രപഞ്ചവും കാലവും, രണ്ടാം പതിപ്പ്, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2009</p></li>
<li><p>മുരളീധരന്‍ സി എം, ഭാഷാസൂത്രണം: പൊരുളും വഴികളും, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2021</p></li>
<li><p>——— മലയാളത്തിന്റെ വൈജ്ഞാനികപദവി, </p>
<p>പ്രസാധനം: സി എം മുരളീധരന്‍, 2022</p></li>
<li><p>മുഹമ്മദലി എന്‍ എം ഡോ., മതവും മനുഷ്യനും, </p>
<p>ചിന്ത പബ്ലിഷേഴ്സ്, തിരുവനന്തപുരം, 2008</p></li>
<li><p>മേനോന്‍ ആര്‍ വി ജി ഡോ., ശാസ്ത്രസാങ്കേതിക വിദ്യകളുടെ ചരിത്രം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2017</p></li>
<li><p>രമ കെ, ശാസ്ത്രവീഥിയിലെ പെണ്‍കരുത്തുകള്‍, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2008</p></li>
<li><p>രാജന്‍ ഗുരുക്കള്‍ ഡോ., നോളജ് ഇക്കണോമിയുടെ കാണാപ്പുറം, </p>
<p>ശാസ്ത്രഗതി, 2021മെയ്, വാല്യം 55 ലക്കം 10</p></li>
<li><p>രാമചന്ദ്രന്‍ വി എസ് ഡോ., മസ്തിഷ്കം കഥ പറയുന്നു, പരിഭാഷ രവിചന്ദ്രന്‍ സി, ഡി സി
ബുക്സ്, കോട്ടയം, 2011</p></li>
<li><p>രാമചന്ദ്രൻപിള്ള എസ്, വിജ്ഞാനസമൂഹവും ഇടതുപക്ഷവും,</p>
<p><a
href="http://cms.deshabhimani.com/articles/s-ramachandran-pilla-youth-summit-akg-study-centre/927984">http://cms.deshabhimani.com/articles/s-ramachandran-pilla-youth-summit-akg-study-centre/927984</a></p></li>
<li><p>രാമചന്ദ്രന്‍ നായര്‍ സി ജി (ചീഫ് എഡി), രസതന്ത്രം ജീവിതമാക്കിയവര്‍, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2012</p></li>
<li><p>——— മദാം ക്യൂറി, കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2013</p></li>
<li><p>രാമന്‍കുട്ടി വി, ഡോ., എപ്പിഡെമിയോളജി: രോഗവ്യാപനത്തിന്റെ ശാസ്ത്രം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2021.</p></li>
<li><p>വേണു കെ, പ്രപഞ്ചവും മനുഷ്യനും, സായാഹ്ന ഫൌണ്ടേഷന്‍, 2019</p></li>
<li><p>വേലായുധന്‍ പി എസ് പ്രൊഫ., ലോകചരിത്രം (ഒന്നാം ഭാഗം), </p>
<p>പതിനാലാം പതിപ്പ്, കേരള ഭാഷാ ഇന്‍സ്റ്റിറ്റ്യൂട്ട്, തിരുവനന്തപുരം, 2005</p></li>
<li><p>ശാന്തകുമാര്‍ സി ജി, എം പി പരമേശ്വരന്‍, നാം ജീവിക്കുന്ന ലോകം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 1985</p></li>
<li><p>ശാസ്ത്രവര്‍ഷം 2009, ശാസ്ത്രക്ലാസ്സുകള്‍ക്കുള്ള കൈപ്പുസ്തകം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2009</p></li>
<li><p>ശാസ്ത്രത്തിന്റെ വികാസവും സാമൂഹ്യമാറ്റവും, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2009</p></li>
<li><p>ശാസ്ത്രാവബോധ കാമ്പെയ്ന്‍- കൈപ്പുസ്തകം, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2014</p></li>
<li><p>ശിവശങ്കരന്‍ എം പ്രൊഫ., ശാസ്ത്രചരിത്രം ജീവചരിത്രങ്ങളിലൂടെ, മൂന്നാം പതിപ്പ്,
കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2004</p></li>
<li><p>——— മനുഷ്യന്റെ ഉല്‍പ്പത്തി, പരിഷ്കരിച്ച മൂന്നാം പതിപ്പ്, </p>
<p>കേരള ശാസ്ത്രസാഹിത്യ പരിഷത്ത്, തൃശൂര്‍, 2011</p></li>
<li><p>ശ്രീധരന്‍ ഇ പ്രൊഫ, കെ പി ദേവദാസ്, ലോകചരിത്രം (രണ്ടാം ഭാഗം), </p>
<p>കേരള ഭാഷാ ഇന്‍സ്റ്റിറ്റ്യൂട്ട്, തിരുവനന്തപുരം, 2005</p></li>
<li><p>ശ്രീനിധി കെ എസ്, സ്വതന്ത്രലഭ്യതാപ്രസ്ഥാനം എന്ന അവകാശപ്പോരാട്ടം, <a
href="https://luca.co.in/open-access-movement/">https://luca.co.in/open-access-movement/</a></p></li>
<li><p>സുരേഷ്കുമാര്‍ ആര്‍ ഡോ, വിജ്ഞാനസമൂഹവും ഉന്നതവിദ്യാഭ്യാസവും,‍ </p>
<p>ശാസ്ത്രഗതി, 2021മെയ്, വാല്യം 55 ലക്കം 10</p></li>
<li><p>Ammon Ulrich, Language Planning for International Scientific
Communication: An Overview of Questions and Potential Solutions, Current
Issues in Language Planning, 7:1, 1-30, DOI: 10.2167/cilp088.0</p></li>
<li><p>——— Linguistic inequality and its effects on participation in
scientific discourse and on global knowledge accumulation –With a closer
look at the problems of the second rank language communities, DOI
10.1515/applirev-2012-0016</p></li>
<li><p>——— Global English and the non-native speaker Overcoming
disadvantage, Language in the Twenty First Century, Tonkin Humphrey,
Reagan Timothy (Edi), John Benjamins Publishing Company, Amsterdam/
Philadelphia, 2003</p></li>
<li><p>Amrit B L S, After NASA Refuses To Rename James Webb Telescope,
Advisor Quits in Protest, <a
href="https://science.thewire.in/spaceflight/nasa-rename-james-webb-telescope-advisor-quit-protest/">https://science.thewire.in/spaceflight/nasa-rename-james-webb-telescope-advisor-quit-protest/</a></p></li>
<li><p>Berlin Declaration on Open Access to Knowledge in the Sciences
and Humanities, <a
href="https://openaccess.mpg.de/Berlin-Declaration">https://openaccess.mpg.de/Berlin-Declaration</a></p></li>
<li><p>Briggs Helen, Gender equality: 'No room at the top for women
scientists',</p>
<p><a
href="https://www.bbc.com/news/science-environment-49552812">https://www.bbc.com/news/science-environment-49552812</a></p></li>
<li><p>Childe Gorden V, Society and knowledge, Akar books, Delhi,
2018</p></li>
<li><p>Cooper L Robert, Language planning and social change, Cambridge
University press, 1989,</p></li>
<li><p>Do Babies Begin Learning Language in the Womb?, <a
href="https://www.medicinenet.com/script/main/art.asp?articlekey=166759">https://www.medicinenet.com/script/main/art.asp?articlekey=166759</a></p></li>
<li><p>Ekanayaka Sally, Open Science &amp; Open Access : How far apart
are they?, <a
href="https://www.mysciencework.com/omniscience/open-science-open-access-far-apart">https://www.mysciencework.com/omniscience/open-science-open-access-far-apart</a></p></li>
<li><p>Finquelievich Susana and Roxana Bassi, The Role of Language in
Knowledge Society Educational Systems, <a
href="http://edutechdebate.org/cultural-heritage-and-role-of-education/the-role-of-launguage-in-knowledge-society-educational-systems/">http://edutechdebate.org/cultural-heritage-and-role-of-education/the-role-of-launguage-in-knowledge-society-educational-systems/</a></p></li>
<li><p>Gordin Michael D, Absolute English: Science once communicated in
a polyglot of tongues, but now English rules alone. How did this happen
– and at what cost?, <a
href="https://aeon.co/essays/how-did-science-come-to-speak-only-english">https://aeon.co/essays/how-did-science-come-to-speak-only-english</a></p></li>
<li><p>——— Translating Textbooks-Russian, German, and the Language
of</p>
<p>Chemistry, <a
href="https://www.journals.uchicago.edu/doi/pdf/10.1086/664980">https://www.journals.uchicago.edu/doi/pdf/10.1086/664980</a></p></li>
<li><p>——— Hydrogen Oxygenovich: Crafting Russian as a language of
science in the late nineteenth century, <a
href="https://static1.squarespace.com/static/5275adb7e4b0298e6ac6bc86/t/565d5945e4b042bda9cc0672/1448958277033/History+of+Science-2015-Gordin-417-37.pdf">https://static1.squarespace.com/static/5275adb7e4b0298e6ac6bc86/t/565d5945e4b042bda9cc0672/1448958277033/History+of+Science-2015-Gordin-417-37.pdf</a></p></li>
<li><p>——— Scientific Babel- How Science Was Done Before and After
Global English, The University of Chicago Press, 2015</p></li>
<li><p>Gray Peter, A Brief History of Education, <a
href="https://www.psychologytoday.com/us/blog/freedom-learn/200808/brief-history-education">https://www.psychologytoday.com/us/blog/freedom-learn/200808/brief-history-education</a></p></li>
<li><p>Haenel T, Superstition, faith, delusion, <a
href="https://pubmed.ncbi.nlm.nih.gov/6669970/">https://pubmed.ncbi.nlm.nih.gov/6669970/</a></p></li>
<li><p>Hamel Rainer Enrique, The dominance of English in the
international scientific periodical literature and the future of
language use in science, DOI: 10.1075/aila.20.06ham</p></li>
<li><p>History of English as a discipline, <a
href="https://resource.acu.edu.au/siryan/Academy/Foundation/English_As_Discipline.htm">https://resource.acu.edu.au/siryan/Academy/Foundation/English_As_Discipline.htm</a></p></li>
<li><p>Hotson Louisa, Social science at the crossroads: the history of
political science in the USA and the evolution of social impact, <a
href="https://blogs.lse.ac.uk/usappblog/2016/08/11/social-science-at-the-crossroads-the-history-of-political-science-in-the-usa-and-the-evolution-of-social-impact/">https://blogs.lse.ac.uk/usappblog/2016/08/11/social-science-at-the-crossroads-the-history-of-political-science-in-the-usa-and-the-evolution-of-social-impact/</a></p></li>
<li><p>Hunt Elgin F., David C. Colander, Social Science: An Introduction
to the Study of Society, Fourteenth Edition, Allyn &amp; Bacon,
2010</p></li>
<li><p>Hutchins John W., Machine translation: a brief history, <a
href="https://aymara.org/biblio/mtranslation.pdf">https://aymara.org/biblio/mtranslation.pdf</a></p></li>
<li><p>Johnson Joseph, Most common languages used on the internet as of
January 2020, by share of internet users, <a
href="https://www.statista.com/statistics/262946/share-of-the-most-common-languages-on-the-internet/">https://www.statista.com/statistics/262946/share-of-the-most-common-languages-on-the-internet/</a></p></li>
<li><p>Kanann K P Dr,Towards a Knowledge-based Economy in Kerala Issues
and Challenges, <a
href="https://www.researchgate.net/publication/348994172">https://www.researchgate.net/publication/348994172</a></p></li>
<li><p>Lytras M D and Sicilia M A (2005) The Knowledge Society: a
manifesto for knowledge and learning, Int. J. Knowledge and Learning,
Vol. 1, Nos. 1/2, pp.1–11</p></li>
<li><p>McElroy Molly, While in womb, babies begin learning language from
their mothers, <a
href="https://www.washington.edu/news/2013/01/02/while-in-womb-babies-begin-learning-language-from-their-mothers/">https://www.washington.edu/news/2013/01/02/while-in-womb-babies-begin-learning-language-from-their-mothers/</a></p></li>
<li><p>Menon R V G, An Introduction to the history and philosophy of
science, Fourth edition, Dorling Kindersley (india) pvt ltd, Chennai
2011</p></li>
<li><p>Meyer Michal, An Element of Order, <a
href="https://www.sciencehistory.org/distillations/an-element-of-order">https://www.sciencehistory.org/distillations/an-element-of-order</a></p></li>
<li><p>Mishra Ram, Science in vernacular language: A boon or a bane, <a
href="https://indiabioscience.org/columns/opinion/science-in-vernacular-language-a-boon-or-a-bane">https://indiabioscience.org/columns/opinion/science-in-vernacular-language-a-boon-or-a-bane</a></p></li>
<li><p>Morata Ginés, The Century of the Gene. Molecular Biology and
Genetics,</p>
<p><a
href="https://www.bbvaopenmind.com/en/articles/the-century-of-the-gene-molecular-biology-and-genetics/">https://www.bbvaopenmind.com/en/articles/the-century-of-the-gene-molecular-biology-and-genetics/</a></p></li>
<li><p>Noordenbos greta, Women in academies of sciences: from exclusion
to exception, Women’s Studies International Forum, Vol. 25, No. 1, pp.
127 – 137, 2002</p></li>
<li><p>Nye Mary Jo, Speaking in Tongues-Science’s centuries-long hunt
for a common language, <a
href="https://www.sciencehistory.org/distillations/speaking-in-tongues">https://www.sciencehistory.org/distillations/speaking-in-tongues</a></p></li>
<li><p>Ofek Hillel, Why the Arabic World Turned Away from Science- On
the lost Golden Age and the rejection of reason, <a
href="https://www.thenewatlantis.com/publications/why-the-arabic-world-turned-away-from-science">https://www.thenewatlantis.com/publications/why-the-arabic-world-turned-away-from-science</a></p></li>
<li><p>Pope John Paul II, Message To The Pontifical Academy Of
Sciences:</p>
<p>On Evolution, <a
href="https://humanorigins.si.edu/sites/default/files/MESSAGE%20TO%20THE%20PONTIFICAL%20ACADEMY%20OF%20SCIENCES%20(Pope%20John%20Paul%20II).pdf">https://humanorigins.si.edu/sites/default/files/MESSAGE%20TO%20THE%20PONTIFICAL%20ACADEMY%20OF%20SCIENCES%20(Pope%20John%20Paul%20II).pdf</a></p></li>
<li><p>Porzucki Nina, How did English become the language of
science?,</p>
<p><a
href="https://www.pri.org/stories/2014-10-06/how-did-english-become-language-science">https://www.pri.org/stories/2014-10-06/how-did-english-become-language-science</a></p></li>
<li><p>Powell Kendall, Ruth Terry, Sophia Chen, How LGBT+ scientists
would like to be included and welcomed in STEM workplaces, <a
href="https://www.nature.com/articles/d41586-020-02949-3">https://www.nature.com/articles/d41586-020-02949-3</a></p></li>
<li><p>Rajan Gurukkal, History and Theory of Knowledge Production an
introductory outline, Oxford University press, 2019</p></li>
<li><p>Ricento Thomas (Edi), Language Policy and Political Economy,
Oxford University Press, 2015</p></li>
<li><p>Ross dorothy, Changing contours of the social science
disciplines, The cambridge history of science,volume 7,The Modern Social
Sciences, Theodore M. Porter and Dorothy Ross (Edi), Cambridge
University Press, 2003</p></li>
<li><p>Scaruffi Piero, A Brief History of Knowledge From 3000 BC to 2001
AD, <a
href="https://www.scaruffi.com/know/history.html">https://www.scaruffi.com/know/history.html</a></p></li>
<li><p>Shesh Babu, Science in Vernacular Languages- Benificial?, <a
href="https://countercurrents.org/2018/06/science-in-vernacular-languages-beneficial/">https://countercurrents.org/2018/06/science-in-vernacular-languages-beneficial/</a></p></li>
<li><p>Shiver Elaine, Brain Development and Mastery of Language in the
Early Childhood Years, <a
href="../Zotero/storage/8JCZCQMT/brain-development-and-mastery-of-language-in-the-early-childhood-years.html">file:///home/murali/Zotero/storage/8JCZCQMT/brain-development-and-mastery-of-language-in-the-early-childhood-years.html</a></p></li>
<li><p>Social science <a
href="https://www.britannica.com/topic/social-science">https://www.britannica.com/topic/social-science</a></p></li>
<li><p>Sooryamoorthy, Wesley Shrum, Is Kerala Becoming a Knowledge
Society? - Evidence from the Scientific Community, <a
href="http://journals.sagepub.com/doi/10.1177/0038022920040203">http://journals.sagepub.com/doi/10.1177/0038022920040203</a></p></li>
<li><p>Stehr Nico and Alexander Ruser, Knowledge Society, Knowledge
Economy and Knowledge Democracy, Handbook of Cyber-Development,
Cyber-Democracy, and Cyber-Defense , Elias G. Carayannis, David F. J.
Campbell, Marios Panagiotis Efthymiopoulos (eds.), Springer
International Publishing</p></li>
<li><p>Szathmáry Eörs and Szabolcs Számadó, Language: a social history
of words, NATURE, Vol 456, 6 November 2008</p></li>
<li><p>Towards Knowledge Societies, Unesco World Report, <a
href="https://unesdoc.unesco.org/ark:/48223/pf0000141843_eng">https://unesdoc.unesco.org/ark:/48223/pf0000141843_eng</a></p></li>
<li><p>Witze Alexandra, NASA won’t rename James Webb telescope — and
astronomers are angry, <a
href="https://www.nature.com/articles/d41586-021-02678-1">https://www.nature.com/articles/d41586-021-02678-1</a></p></li>
<li><p>Wright Sue, Language Policy and Language Planning-From
Nationalism to Globalisation 2nd edition, Palgrave Macmillan,
2016</p></li>
<li><p><a
href="https://en.wikipedia.org/">https://en.wikipedia.org/</a></p></li>
<li><p><a
href="https://ml.wikipedia.org/">https://ml.wikipedia.org</a></p></li>
<li><p><a
href="https://ml.wiktionary.org/wiki">https://ml.wiktionary.org/wiki</a></p></li>
<li><p><a
href="https://www.history.com/">https://www.history.com</a></p></li>
<li><p><a
href="https://www.britannica.com/">https://www.britannica.com</a></p></li>
</ul>

<nav class="pagination pager flat">
    <ul>
        <li class="prev">
            <span class="label tag">Prev</span>
             <a href="../18_vyjnanikavinimayavum_bhashasoothranavum">വൈജ്ഞാനികവിനിമയവും ഭാഷാസൂത്രണവും</a>
        </li>
        <li class="next">
            <span class="label tag">Next</span>
            <a href="../20_anubandam">അനുബന്ധങ്ങൾ</a>
        </li>
    </ul>
</nav>
{{< /rawhtml >}}
