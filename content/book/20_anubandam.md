+++
title = "അനുബന്ധങ്ങൾ"
description = ""
weight = 200
draft = false
toc = true
bref = ""
+++

{{< rawhtml >}}

<ol>
<li><a href="../anubandham01">ഉത്തരത്തിലേക്കുള്ള വഴികള്‍
സി എം മുരളീധരന്‍</a></li>
<li><a href="../anubandham02">അജ്ഞാത ശാസ്ത്രകാരന്മാര്‍
പി മധുസൂദനന്‍</a></li>
<li><a href="../anubandham03">വിവിധ ശാസ്ത്ര അക്കാദമികളില്‍
 സ്ത്രീകള്‍ അംഗങ്ങളായ കാലം</a></li>
 <li><a href="../anubandham04">ബർലിൻ പ്രഖ്യാപനം</a></li>
 <li><a href="../anubandham05">സ്വതന്ത്ര ലഭ്യതയിലുള്ള പുസ്തകങ്ങളും 
ജേണലുകളും ലഭിക്കുന്ന ചില സൈറ്റുകള്‍</a></li>
</ol>

<nav class="pagination pager flat">
    <ul>
        <li class="prev">
            <span class="label tag">Prev</span>
             <a href="../19_avalambam">അവലംബം</a>
        </li>
        <li class="next">
            <span class="label tag">Next</span>
            <a href="../anubandham01">1 ഉത്തരത്തിലേക്കുള്ള വഴികള്‍</a>
        </li>
    </ul>
</nav>
{{< /rawhtml >}}
