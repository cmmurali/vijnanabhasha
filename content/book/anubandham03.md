+++
title = "3 വിവിധ ശാസ്ത്ര അക്കാദമികളില്‍ സ്ത്രീകള്‍ അംഗങ്ങളായ കാലം"
description = ""
weight = 230
draft = false
toc = true
bref = ""
+++

{{< rawhtml >}}

<p>നം. സ്ഥാപനം വര്‍ഷം</p>
<p>1 The Royal Swedish Academy of Sciences 1748</p>
<p>2 The Royal Academy of Letters History and Antiquities 1793</p>
<p>3 The American Academy of Natural Sciences 1841</p>
<p>4 The American Academy of Arts and Sciences 1848</p>
<p>5 The National Academy of Sciences in the Ukraine 1921</p>
<p>6 The National Academy of Science in America 1925</p>
<p>7 Akademie der Wissenschaften Göttingen 1926</p>
<p>8 The Icelandic Academy of Sciences 1928</p>
<p>9 The British Academy 1931</p>
<p>10 Yugoslav Academy of Sciences and Arts (in 1991, the Croatian
Academy of Sciences and Arts) 1936</p>
<p>11 The Russian Academy of Sciences 1939</p>
<p>12 The Royal Society of London (1748): Eva Ekblad 1945</p>
<p>13 Estonian Academy of Sciences 1946</p>
<p>14 The Academia Scientiarum Fennica 1949</p>
<p>15 The Royal Irish Academy 1949</p>
<p>16 Akademie der Wissenschaften und der Literatur, Mainz, Germany
1949</p>
<p>17 The Hungarian Academy of Sciences 1949</p>
<p>18 The Royal Dutch Academy of Sciences 1950</p>
<p>19 Latvian Academy of Sciences 1951</p>
<p>20 The Georgian Academy of Sciences 1955</p>
<p>21 Academie Royale des Sciences des Lettres et des Beaux Arts de
Belgique 1957</p>
<p>22 The Finnish Society of Science and Letters 1960</p>
<p>23 The Bulgarian Academy of Sciences 1961</p>
<p>24 The Royal Academy of Letters History and Antiquity Stockholm
1963</p>
<p>25 Berlin-Brandenburgische Akademie der Wissenschaften 1964</p>
<p>26 Academy of Athens 1967</p>
<p>27 The Royal Danish Academy 1968</p>
<p>28 The Academy of Sciences of Moldova 1970</p>
<p>29 The Royal Swedish Academy of Engineering 1970</p>
<p>30 Académie des Sciences Morales et Politiques 1971</p>
<p>31 Royal Academy of Sciences Letters and Arts in Belgium 1973</p>
<p>32 Pontifica Academica of Sciences 1974</p>
<p>33 Israël Academy of Sciences and Humanities 1974</p>
<p>34 Académie des Inscriptions et Belles Lettres 1975</p>
<p>35 Slovenian Academy of Sciences and Arts 1976</p>
<p>36 The Swiss Academy of Humanities and Social Sciences 1976</p>
<p>37 The Royal Swedisch Academy of Agriculture and Forestry 1977</p>
<p>38 European Academy of Sciences Arts, Humanities 1979</p>
<p>39 Académie des Sciences in France 1980</p>
<p>40 Académie Francaise 1980</p>
<p>41 Academia Europea 1988</p>
<p>42 Royal Swedish Academy of Engineering 1990</p>
<p>43 European Academy of Sciences and Arts 1991</p>
<p>44 The Turkish Academy of Sciences 1993</p>
<p>45 Pontifica Academica of Social Sciences 1994</p>
<p>46 Bayerische Akademie der Wissenschaften 1995</p>
<p>47 Real Academia de Science et Morales y Politicas Madrid 1995</p>
<p>(കടപ്പാട്: Greta Noordenbos, women in academies of sciences: from
exclusion to exception)</p>
<nav class="pagination pager flat">
    <ul>
        <li class="prev">
            <span class="label tag">Prev</span>
             <a href="../anubandham02">1 ഉത്തരത്തിലേക്കുള്ള വഴികള്‍</a>
        </li>
        <li class="next">
            <span class="label tag">Next</span>
            <a href="../anubandham04">4 ബർലിൻ പ്രഖ്യാപനം</a>
        </li>
    </ul>
</nav>
{{< /rawhtml >}}
