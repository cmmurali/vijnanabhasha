+++
title = "അറിവും ഭാഷയും"
description = ""
weight = 30
draft = false
toc = true
bref = ""
+++

{{< rawhtml >}}

<p>മനുഷ്യപൂര്‍വികരുടെ സഹകരണവും കൂട്ടായ ജീവിതവും കൂടുതല്‍ ഫലപ്രദമായി തുടങ്ങിയ നാളുകളിലാണ്
അവര്‍ക്ക് പരസ്പരം എന്തൊക്കെയോ പറയണം എന്ന് വന്നത്. ആംഗ്യഭാഷയോടെയായിരുന്നു തുടക്കം. അത്
പിന്നീട് ചില ശബ്ദങ്ങളുടെ അകമ്പടിയോടു കൂടിയായി. ഇരയെ കണ്ടെത്തിയ വിവരം ഒരു പ്രത്യേക
ശബ്ദത്തിലൂടെ മറ്റുള്ളവരുടെ ശ്രദ്ധയിലേക്ക് കൊണ്ടുവരാന്‍ ആദ്യം അവര്‍ പഠിച്ചിട്ടുണ്ടാവും. പക്ഷേ,
ഇതാ ഇവിടെ ഇരയുണ്ട്, എന്നാല്‍ അപ്പുറത്ത് മരത്തിന്റെ മറവില്‍ ഒരു സിംഹം പതിയിരുപ്പുണ്ട് എന്ന്
മറ്റുള്ളവരോട് പറയാന്‍ ആ ശബ്ദത്തിലൂടെ കഴിയുമായിരുന്നില്ല.</p>
<p>എല്ലാ ജീവികളും കേവലമായ രീതിയില്‍ ആശയവിനിമയം നടത്താറുണ്ട്. അതിന് അവയ്ക്ക് അവയുടെതായ
രീതികളുമുണ്ട്. ആശയവിനിമയത്തിനായി അമറലോ, മുരളലോ, മൂളലോ ഒക്കെ അവ പല തരത്തില്‍
പ്രയോജനപ്പെടുത്തുന്നു. വീട്ടിലെ നായകളെ സൂക്ഷ്മനിരീക്ഷണത്തിന് വിധേയമാക്കിയാല്‍ അവയുടെ ചില
ആശയവിനിമയരീതികള്‍ മനസ്സിലാക്കിയെടുക്കാം. വിശക്കുമ്പോഴും ശത്രുവിനെ കാണുമ്പോഴും സ്നേഹം
പ്രകടിപ്പിക്കാനുമൊക്കെ അവ വ്യത്യസ്തരീതിയിലുള്ള ശബ്ദങ്ങളുപയോഗിക്കുന്നു. തേനീച്ചയുടെ
നൃത്തത്തിന്റെ വിവിധ രൂപങ്ങള്‍ അവ കണ്ടെത്തിയ തേനിന്റെ അളവ്, പോകേണ്ട ദിശ എന്നിവയൊക്കെ
സൂചിപ്പിക്കുന്നവയാണ്. മനുഷ്യപൂര്‍വികരും അക്കാലത്ത് ഇങ്ങനെ വ്യത്യസ്ത കാര്യങ്ങള്‍ക്കായി വ്യത്യസ്തതരം
വിളികള്‍ ഉപയോഗിച്ചിട്ടുണ്ടാവും. പക്ഷേ, കൂടുതല്‍ കാര്യങ്ങള്‍ ഒരുമിച്ച് അവതരിപ്പിക്കേണ്ട സമയത്ത്
പരിമിതമായ വിളിരൂപങ്ങള്‍ മതിയാവാതെ വരികയും അവര്‍ പുറപ്പെടുവിക്കുന്ന ശബ്ദങ്ങളിലെ ഖണ്ഡങ്ങള്‍ക്ക്
നിശ്ചിത അര്‍ത്ഥം ആവര്‍ത്തനത്തിലൂടെ ഉറപ്പിക്കുകയുമായിരിക്കും ചെയ്തിട്ടുണ്ടാവുക. പിന്നീട്
നിശ്ചിതാര്‍ത്ഥമുള്ള ഈ ഖണ്ഡങ്ങളെ കൂട്ടിച്ചേര്‍ത്തുകൊണ്ട് അവര്‍ ആശയവിനിമയം ഫലപ്രദമാക്കാന്‍
ശ്രമിച്ചിട്ടുണ്ടാവും. ശബ്ദമുണ്ടാക്കുന്ന ആളും കേള്‍ക്കുന്ന ആളും ഒരേ അര്‍ത്ഥം കല്‍പ്പിച്ചാലേ ഈ രീതി
ഫലപ്രാപ്തിയിലെത്തൂ. നിരവധി സന്ദര്‍ഭങ്ങളിലെ ഉപയോഗവും കൂടെയുള്ള ആംഗ്യങ്ങളുമെല്ലാം
ചേര്‍ന്നായിരിക്കും ഈ തരത്തിലുള്ള ആശയവിനിമയം രൂപപ്പെട്ടു വന്നിട്ടുണ്ടാവുക.</p>
<p>ഹോമിനിഡുകളുടെ (ആധുനിക മനുഷ്യരും ബന്ധപെട്ട ഉപവർഗങ്ങളും ഉള്‍പ്പെടുന്ന ജനുസ്)
കാലമാവുമ്പോഴേക്കും പ്രൈമേറ്റുകളുടെ (മനുഷ്യര്‍, കുരങ്ങുകള്‍ തുടങ്ങിയ സസ്തനികള്‍ ഉള്‍‍പ്പെടുന്ന ഓര്‍ഡര്‍)
കാലത്തെ അപേക്ഷിച്ച് അവര്‍ക്ക് വിനിമയം ചെയ്യേണ്ട ആവശ്യങ്ങളും വസ്തുതകളും കൂടിവന്നു. അവര്‍ക്ക്
ആശയവിനിമയ സമ്പ്രദായം വികസിപ്പിച്ചേ പറ്റുമായിരുന്നുള്ളു. ശബ്ദങ്ങളെ തരംതിരിക്കുന്നതിലും
വ്യാഖ്യാനിക്കുന്നതിലുമെല്ലാം മെച്ചപ്പെട്ട നിലയിലേക്ക് കടക്കുന്നതിന് ഇതു കാരണമായിരിക്കും.
വിളിയുടെ പൂര്‍ണരൂപത്തെക്കാള്‍ അതിന്റെ ഘടകങ്ങളില്‍ ശ്രദ്ധ ചെലുത്തുന്നത് ആവശ്യമായി വന്നു. പരസ്പരം
മിശ്രണം നടത്തിയ വിളികളും ഈ ഘട്ടത്തില്‍ കൂടുതലായി രൂപപ്പെട്ടു. ഇവയെല്ലാം സൂക്ഷ്മമായി
വിശകലനം ചെയ്യാന്‍ പര്യാപ്തമായ രീതിയിലേക്ക് മസ്തിഷ്കത്തിലും നാഡീവ്യൂഹങ്ങളിലും മാറ്റങ്ങളും‍
വന്നുചേര്‍ന്നു. ഭാഷയും ബുദ്ധിയും ഒന്നിച്ചാണ് വളര്‍ന്നതെന്നാണ് ഇതിന്റെ അര്‍ത്ഥം. പദങ്ങള്‍ കൂടിവരുന്ന
ഈ വേളയില്‍ വ്യാകരണത്തിന്റെ ആദ്യരൂപങ്ങളും ഉടലെടുത്തേ പറ്റു. പതിനായിരക്കണക്കിന്
വര്‍ഷങ്ങളിലൂടെയാണ് ഈ മാറ്റങ്ങള്‍ ഉണ്ടായിവന്നത്.</p>
<p>ശബ്ദനാളികളുടെ ഘടനയില്‍ വന്നുചേര്‍ന്ന മാറ്റമാണ് മറ്റൊരു ഘടകം. നിവര്‍ന്നുനില്‍ക്കാന്‍
തുടങ്ങിയതിനനുസരിച്ച് തലയോട് നട്ടെല്ലിനു മുകളില്‍ തുലനം ചെയ്യാനായി ചില മാറ്റങ്ങള്‍
ഉണ്ടായതിനെ തുടര്‍ന്ന് ശബ്ദനാളിയുടെ ആകൃതിയില്‍ ചെറിയ മാറ്റം ഉണ്ടായി. ഒപ്പം
കൃക(Glottis)വും മൃദുതാലുവും തമ്മിലുള്ള അകലവും വര്‍ധിച്ചു. മനുഷ്യന്റെ ഉല്പത്തി എന്ന പുസ്തകത്തില്‍
പ്രൊഫ. എം ശിവശങ്കരന്‍ ഇങ്ങനെ ചൂണ്ടിക്കാട്ടുന്നു.</p>
<p>"ശബ്ദനാളിയുടെ നീളം കൂടിയതുകൊണ്ടാണ് ഗ്രസനീനാളിയുടെ അനുനാദ (resonance)
സാധ്യതകളെല്ലാം പുറത്തുകൊണ്ടുവരാന്‍ കഴിഞ്ഞത്. ഗ്രസനിയുടെ വലുപ്പത്തില്‍ വലിയ മാറ്റങ്ങള്‍
വരുത്തുവാനുള്ള കഴിവും മനുഷ്യന്റെ സവിശേഷതയാണ്. അതിന്റെ ഏറ്റവും കൂടിയ വീതി ഏറ്റവും
കുറഞ്ഞതിനേക്കാള്‍ പത്തിരട്ടിയാക്കാന്‍ നമുക്ക് കഴിയും. ശബ്ദോച്ചാരണത്തിലെ സുപ്രധാന
ഘടകങ്ങളാണിതെല്ലാം. ചില സംഗീതോപകരണങ്ങളിലെ പല നീളത്തിലും വ്യാസത്തിലുമുള്ള കുഴലുകളുടെ
കാര്യമാണിതു ചെയ്യുന്നത്."</p>
<p>ശബ്ദനാളിയിലുണ്ടായ മാറ്റങ്ങളോടൊപ്പം മസ്തിഷ്കത്തിലുണ്ടായ മാറ്റങ്ങളും പരസ്പരം
പരിപോഷിപ്പിച്ചപ്പോള്‍ മനുഷ്യരുടെ ഭാഷാപരമായ കഴിവുകള്‍ വികസിച്ചു. അധ്വാനവും ഭാഷയും ചേര്‍ന്ന്
തലച്ചോറിനെ അതിന്റെ പൂര്‍ണവികാസത്തിലേക്ക് നയിച്ചു. നായാട്ടിലേക്ക് കടന്നതോടെ
ഭക്ഷണകാര്യത്തിലുണ്ടായ മാറ്റവും മനുഷ്യവളര്‍ച്ചയെയും അതുവഴി മസ്തിഷ്കത്തിന്റെ വളര്‍ച്ചയെയും
പോഷിപ്പിക്കുന്നുണ്ട്.</p>
<p><img src="/Pictures/chp03_01.jpg"
style="width:16.51cm;height:15.593cm" /></p>
<p>ചിത്രം 3.1- മനുഷ്യരുടെ ഉച്ചാരണാവയവങ്ങള്‍</p>
<p>(കടപ്പാട് https://www.researchgate.net)</p>
<p>ന്യൂറോസയന്‍സിന്റെ കണ്ടെത്തലുകള്‍</p>
<p>ഭാഷോല്പത്തിയെക്കുറിച്ചുള്ള ധാരണകളെ പുതിയ തലങ്ങളിലേക്ക് നയിക്കാന്‍ ആധുനിക
ന്യൂറോസയന്‍സിന്റെ അന്വേ‍ഷണങ്ങള്‍ക്ക് കഴിഞ്ഞിട്ടുണ്ട്. ഇന്ത്യക്കാരനായ ഡോ. വി എസ് രാമചന്ദ്രന്‍ ഈ
രംഗത്തെ ഗവേഷണങ്ങളിലൂടെ പ്രശസ്തനായ വ്യക്തിയാണ്. അദ്ദേഹത്തിന്റെ ദി ടെല്‍ ടെയ്‌ല്‍ ബ്രെയ്ന്‍
(The Tell-Tale Brain) എന്ന പുസ്തകത്തില്‍ ഭാഷാപരിണാമത്തെക്കുറിച്ച് വിശദമായി
പ്രതിപാദിക്കുന്നുണ്ട്. ആദിഭാഷയായ ആംഗ്യഭാഷയാണ് സംസാരഭാഷയുടെ ഉത്ഭവത്തിന് കാരണമെന്ന്
അദ്ദേഹം അഭിപ്രായപ്പെടുന്നു. ഒരു വലിയ നാഡീബന്ധജാലികയുടെ ഏകോപിതമായ പ്രവര്‍ത്തനത്തിന്റെ
ഫലമാണ് ഭാഷാനിര്‍മാണം. ഭാഷയെ ഒരൊറ്റ ധര്‍മമായി കണക്കാക്കുന്ന നമ്മുടെ കാഴ്ചപ്പാട് ശരിയല്ല.
വാക്കുകള്‍, അര്‍ത്ഥം, വ്യാകരണം എന്നിങ്ങനെ വാചകത്തിന് മൂന്ന് ഘടകങ്ങളുണ്ട്. ഇവ വ്യത്യസ്തമാണെന്ന്
തോന്നാത്തവിധം ഇഴചേര്‍ന്ന് കിടക്കുകയാണെന്നേയുള്ളൂ. ഇത്തരം വ്യത്യസ്തഘടകങ്ങളെ നിയന്ത്രിക്കുന്നതിന്
പ്രത്യേകം ഭാഗങ്ങള്‍ മസ്തിഷ്കത്തില്‍ ഉണ്ടാവാന്‍ സാധ്യതയുണ്ട് എന്ന് കരുതേണ്ടിയിരിക്കുന്നു. പക്ഷേ,
ഇതിന്റെ വിശദാംശങ്ങളില്‍ യോജിപ്പിലേക്ക് കടക്കാന്‍ ഗവേഷകര്‍ക്ക് കഴിഞ്ഞിട്ടില്ല. എങ്കിലും
പദവിന്യാസഘടന ബ്രോക്കാസ് മേഖലയുമായും അര്‍ത്ഥവിജ്ഞാനീയം ഇടതു ടെമ്പറല്‍ ലോബുമായും വ്യാകരണം
വെര്‍നിക്കസ് മേഖലയുമായും ബന്ധപ്പെട്ടാണ് എന്ന് കരുതാം. ഭാഷയെ ബന്ധിപ്പിച്ച മസ്തിഷ്കമേഖലകളെ
മനസ്സിലാക്കിയിട്ട് നൂറു വര്‍ഷത്തോളമായെങ്കിലും നിരവധി പ്രശ്നങ്ങള്‍ക്ക് ഇനിയും ഉത്തരം
കണ്ടെത്തേണ്ടതുണ്ട്.</p>
<p>മറ്റുജീവികളുടെ ആശയവിനിമയ ഉപാധികളില്‍ നിന്ന് മനുഷ്യഭാഷയെ വ്യതിരിക്തമാക്കുന്ന അഞ്ച്
ഘടകങ്ങളെ വി എസ് രാമചന്ദ്രന്‍ ഇങ്ങനെ രേഖപ്പെടുത്തുന്നു.</p>
<p>ഒന്ന്- നമ്മുടെ പദസമ്പത്തിന്റെ വൈപുല്യം. ചുരുങ്ങിയത് അറുനൂറ് വാക്കുകളുടെയെങ്കിലും ഉടമയാണ്
എട്ടു വയസ്സുള്ള ഒരു കുട്ടിപോലും. മെച്ചപ്പെട്ട ഓര്‍മശക്തിയായിരിക്കാം നമ്മെ ഇതിന്
സഹായിക്കുന്നത്.</p>
<p>രണ്ട്- കൂട്ടിയോജിപ്പിക്കാന്‍ കഴിയുന്ന വാക്കുകള്‍. നിര്‍വഹണ പദങ്ങള്‍ അഥവാ ഫങ്ഷന്‍ വേര്‍ഡ്സ്
(വ്യാകരണത്തില്‍ ദ്യോതകപദങ്ങള്‍ എന്നു പറയുന്നവ) മനുഷ്യഭാഷയുടെ പ്രത്യേകതയാണ്. ഇത്തരം വാക്കുകള്‍ക്ക്
ഭാഷാപരമായ ഈ ധര്‍മമല്ലാതെ സ്വതന്ത്രമായ അസ്തിത്വമില്ല.</p>
<p>മൂന്ന്- ഭൂത- ഭാവി കാലങ്ങളെ പരാമര്‍ശിക്കാന്‍ നമ്മുടെ ഭാഷയ്ക്കു കഴിയുന്നു. മൃഗങ്ങള്‍ക്ക് ഇത്തരം
സങ്കീര്‍ണതകളെ ആശയവിനിമയത്തില്‍ കൊണ്ടുവരാന്‍ കഴിയില്ല.</p>
<p><img src="/Pictures/chp03_02.jpg"
style="width:16.51cm;height:8.361cm" /></p>
<p>ചിത്രം 3. 2- സംവേദക കോര്‍ട്ടക്സിന്റെ ചിത്രീകരണം- വിവിധ അവയവങ്ങള്‍ കോര്‍ട്ടക്സില്‍
പ്രതിനിധാനം ചെയ്യുന്ന താരതമ്യ അളവ്. കൈകള്‍, ചുണ്ടുകള്‍ എന്നിവയുടെ പ്രാധാന്യം കാണുക.
(കടപ്പാട് https://www.ebmconsult.com)</p>
<p>നാല്- രൂപകം, ശ്ലേഷോക്തികള്‍ എന്നിവയെല്ലാം മനുഷ്യഭാഷയുടെ മാത്രം പ്രത്യേകതയാണ്.</p>
<p>അഞ്ച്- വഴക്കവും ആവര്‍ത്തനസ്വഭാവവുമുള്ള പദവിന്യാസഘടന.</p>
<p>ഈ പ്രത്യേകതകളില്‍ ആദ്യത്തെ നാലെണ്ണത്തെ ചേര്‍ത്ത് ഇന്നത്തെ ഭാഷയുടെ പ്രാഗ്‌രൂപം അഥവാ
ആദിമഭാഷ (protolanguage) എന്ന് വിശേഷിപ്പിച്ചത് ഡെറക്ക് ബിക്കര്‍ടണ്‍ എന്ന
ഭാഷാശാസ്ത്രജ്ഞനാണ്.</p>
<p>ബോധം, ഭാഷയുടെ പരിണാമം എന്നീ രണ്ട് കാര്യങ്ങള്‍ എത്രയോ കാലമായി അന്വേഷകരുടെ
സവിശേഷമായ ശ്രദ്ധയെ ആകര്‍ഷിച്ചുപോരുന്നു. ഭാഷയുടെ ഉല്പത്തിയെപ്പറ്റി നിരവധി സിദ്ധാന്തങ്ങള്‍
മുന്നോട്ടുവെക്കപ്പെട്ടെങ്കിലും അവയില്‍ ഭൂരിപക്ഷവും ഒരടിസ്ഥാനവുമില്ലാത്തവയായിരുന്നു. ഇത്തരം
സിദ്ധാന്തങ്ങളില്‍ ഗതികെട്ടാണ് പാരീസിലെ ഭാഷാശാസ്ത്ര അക്കാദമി 1866 ലും 1911 ലുമായി
രണ്ടുതവണ, ഭാഷയുടെ ഉല്പത്തിയെക്കുറിച്ചുള്ള ചര്‍ച്ചകള്‍ തന്നെ നിരോധിച്ചത്. ഈജിപ്തിലെ
ഫറോവയായിരുന്ന സാമെത്തിക്കസും പിന്നീട് നൂറ്റാണ്ടുകള്‍ക്കു ശേഷം സ്കോട്ട്ലാന്‍ഡിലെ ജെയിംസ്
നാലാമന്‍ രാജാവും ലോകത്തിലെ ആദ്യഭാഷയേതെന്ന് കണ്ടെത്താന്‍ നടത്തിയ പരീക്ഷണങ്ങള്‍
ക്രൂരമായിരുന്നു. രണ്ട് ശിശുക്കളെ മനുഷ്യശബ്ദമൊന്നും കേള്‍ക്കാതെ വളര്‍ത്തുകയായിരുന്നു സാമെത്തിക്കസ്
ചെയ്തത്. ജെയിംസ് നാലാമന്‍ ഈ പരീക്ഷണം ആവര്‍ത്തിച്ചു. ഭാഷാശാസ്ത്രജ്ഞരാവട്ടെ ഭാഷയുടെ തുടക്കം
എങ്ങനെയായിരുന്നു എന്നതിനേക്കാള്‍ അതിന്റെ ആന്തരികനിയമങ്ങളുടെ സങ്കീര്‍ണതകള്‍ തേടുന്നതിലായിരുന്നു
ശ്രദ്ധിച്ചത്. ഭാഷയുടെ ഘടനയെ പരിശോധിക്കുന്നതില്‍ അവര്‍ മുഴുകി.</p>
<p>ഭാഷാപരിണാമത്തെക്കുറിച്ച് ആല്‍ഫ്രഡ് വാലസ്, നോം ചോംസ്കി, സ്റ്റീഫന്‍ ജെയ് ഗൂള്‍ഡ്, സ്റ്റീവന്‍
പിങ്കര്‍ എന്നിവര്‍ മുന്നോട്ടുവച്ച ആശയങ്ങളെ പരിശോധിച്ച ശേഷം പുതിയൊരു ചട്ടക്കൂട് വി എസ്
രാമചന്ദ്രന്‍ മുന്നോട്ടുവച്ചു. ചോംസ്കിയുടെയും പിങ്കറുടെയും ചില ആശയങ്ങളെ താന്‍
പ്രയോജനപ്പെടുത്തുന്നുണ്ടെന്ന് സൂചിപ്പിച്ചുകൊണ്ട് അദ്ദേഹം മുന്നോട്ടുവച്ച സിദ്ധാന്തം സിനസ്തെറ്റിക്ക്
ബൂട്ട് സ്ട്രാപ്പിങ് സിദ്ധാന്തം (synesthetic bootsrapping theory) എന്നാണ്
അറിയപ്പെടുന്നത്. പാര്‍ശ്വാനുകൂലനത്തിലൂടെ (exaptation principle- ഒരു പ്രത്യേക
പ്രവൃത്തിക്കുള്ള അനുകൂലനം വ്യത്യസ്തമായ മറ്റൊരു പ്രവൃത്തിക്ക് വേണ്ടി പ്രയോജനപ്പെടുത്തുക)
ഉരുത്തിരിഞ്ഞതാണ് അമൂര്‍ത്തചിന്തയുടെ പല ഘടകഗുണങ്ങളെന്നും, നവീനമായ ഫലങ്ങളെ സൃ‍ഷ്ടിച്ചത്
അവിചാരിതമായ സംയോജനങ്ങളാണെന്നും അദ്ദേഹം ചൂണ്ടിക്കാട്ടുന്നു.</p>
<p>ഭാഷ പ്രകൃത്യാ ഉള്ളതാണോ, അതോ അഭ്യാസത്തിലൂടെ കൈവരിക്കുന്നതാണോ, എന്ന ചോദ്യത്തിന്
അദ്ദേഹം ഇങ്ങനെ ഉത്തരം പറയുന്നു.</p>
<p>"നിയമങ്ങള്‍ ആര്‍ജിക്കാനുള്ള ശേഷി നൈസര്‍ഗികമാണ്. പക്ഷേ, സ്വായത്തമാക്കാന്‍ അവസരങ്ങള്‍
തുറക്കപ്പെടണം. ഇവിടെ നിയമം ആര്‍ജിക്കാനുള്ള ഈ ശേഷി സമ്മാനിക്കുന്നത് ഇന്നും അജ്ഞാതമായ ഭാഷ
ആര്‍ജിക്കാനുള്ള വ്യവസ്ഥയാണ് (Language Acquisition Device- LAD). മനുഷ്യര്‍ക്ക് ഈ ‍ LAD
ഉണ്ട്; ആള്‍ക്കുരങ്ങുകള്‍ക്കില്ല."</p>
<p>തുടര്‍ച്ചയായി പരിശീലിപ്പിച്ചാലും ആള്‍ക്കുരങ്ങുകള്‍ക്ക് ഭാഷ അഭ്യസിക്കാനാവില്ല. അതേ
അവസരത്തില്‍ ഭാഷ ആര്‍ജിക്കുന്നതില്‍ നിന്ന് മനുഷ്യക്കുട്ടികളെ തടയാന്‍ പ്രയാസവുമാണ്. എന്നാല്‍ ഈ
വിഷയത്തെ അതിലളിതവല്‍ക്കരണത്തിന് വിധേയമാക്കി സമീപിക്കാന്‍ കഴിയില്ല എന്ന് ഓര്‍ക്കണം.
കച്ചവടത്തിലെ ലാഭനഷ്ടക്കണക്കുകളെക്കുറിച്ച് ഉത്തരം പറയുന്നതുപോലെ ഭാഷയുടെ ഉല്പത്തിയുമായി
ബന്ധപ്പെട്ട ചോദ്യങ്ങള്‍ക്ക് ഉത്തരം കണ്ടെത്താന്‍ കഴിയില്ല. </p>
<p>ആദിമഭാഷ അഥവാ ആംഗ്യഭാ‍ഷ സംസാരഭാഷയിലേക്ക് മാറിവന്നത് എങ്ങനെയായിരിക്കും? അതില്‍
സിന്‍കിനേഷ്യയ്ക്ക് നിര്‍ണായകമായ പങ്കാണുള്ളത്. എന്താണ് സിന്‍കിനേഷ്യ? സിന്‍ എന്നാല്‍ കൂടെ, കിനേഷ്യ
എന്നാല്‍ ചലനം, അതായത് ഒന്നിച്ചുള്ള ചലനം. ആംഗ്യങ്ങളെ തുടര്‍ച്ചയായി വാക്കുകളിലേക്ക് വിവര്‍ത്തനം
ചെയ്യാനുള്ള മസ്തിഷ്കത്തിന്റെ സിദ്ധിയെയാണ് സിന്‍കിനേഷ്യ എന്നു വിളിക്കുന്നത്. വൈകാരികമായ
ശബ്ദങ്ങള്‍ സൃഷ്ടിക്കുമ്പോള്‍ കൈകളും കാലുകളുമെല്ലാമുപയോഗിച്ച് അവയ്ക്ക് അനുപൂരകമായ ആംഗ്യങ്ങള്‍
സൃഷ്ടിക്കുന്നു. ഇത് വാക്കുകളുടെ പിറവിയിലേക്ക് നയിക്കുന്നു.</p>
<p><img src="/Pictures/chp03_03.jpg"
style="width:16.51cm;height:12.241cm" /></p>
<p>ചിത്രം 3.3- മസ്തിഷ്കത്തിലെ പ്രധാന ഭാഷാമേഖലകള്‍</p>
<p>(കടപ്പാട് : വി എസ് രാമചന്ദ്രന്‍, മസ്തിഷ്കം കഥ പറയുന്നു)</p>
<p>ആംഗ്യഭാഷ സംസാരഭാഷയായി പരിണമിച്ചതിന്റെ മൂര്‍ത്തമായ ഒരുദാഹരണമായി ഇവിടെ വരൂ, പോകൂ
എന്നീ വാക്കുകളെ വി എസ് രാമചന്ദ്രന്‍ ചൂണ്ടിക്കാണിക്കുന്നു. ഇവിടെ വരൂ എന്ന് പറയുമ്പോള്‍ കൈത്തലം
ഉയര്‍ത്തിപ്പിടിക്കുകയും വിരലുകള്‍ സംസാരിക്കുന്ന ആളുടെ ദിശയിലേക്ക് മടക്കുകയും ചെയ്യുന്നു.
നാക്കിന്റെ ചലനവും ഇതേ രീതിയിലാണ് വരുന്നത്. വരൂ എന്നു പറയുമ്പോള്‍ നാക്കും ചുണ്ടും അകത്തേക്ക്
വളയുന്നു. പോകൂ എന്ന് പറയുമ്പാള്‍ നാക്കു പുറത്തേക്കു തള്ളുകയാണ് ചെയ്യുന്നത്. ഭാഷാസമ്പത്തിന്റെ
ആദ്യരൂപങ്ങള്‍ സിന്‍കിനേഷ്യയുമായി ബന്ധപ്പെട്ടതാണെന്നതിന്റെ തെളിവായി ഇതിനെ കാണാം.</p>
<p><img src="/Pictures/chp03_04.jpg"
style="width:16.51cm;height:13.406cm" /></p>
<p>ചിത്രം 3.4 - ഭാഷയുടെ പരിണാമത്തെ ത്വരിതപ്പെടുത്തിയ മസ്തിഷ്കഭാഗങ്ങള്‍</p>
<p>B-ബ്രോക്കാസ് മേഖല, A-ശ്രവ്യ കോര്‍ട്ടക്സ്, W-വെര്‍ണിക്കസ് മേഖല, AG-ആംഗുലാര്‍ ജെനുസ്,
H-കൈയുടെ മോട്ടോര്‍ കോര്‍ട്ടക്സിലെ ഭാഗം, IT-ഇന്‍ഫെറോ ടെംപറല്‍ കോര്‍ട്ടക്സ്. (കടപ്പാട് : വി
എസ് രാമചന്ദ്രന്‍, മസ്തിഷ്കം കഥ പറയുന്നു)</p>
<p>വളരെ പെട്ടെന്നാണ് ഭാഷ പരിണമിക്കുന്നത് എന്നും അദ്ദേഹം ചൂണ്ടിക്കാട്ടുന്നു. ഒരു ഭാഷ
പൂര്‍ണമായും മാറിമറിയാന്‍ കേവലം ഇരുനൂറു വര്‍ഷങ്ങളൊക്കെ മതിയാവും. ഒരേ ഭാഷ സംസാരിക്കുന്ന
ഒരാളും അയാളുടെ മുതു- മുതു- മുത്തശ്ശിക്കും ആശയവിനിമയം അസാധ്യമായിരിക്കാന്‍ സാധ്യതയുണ്ട്.
പദവിന്യാസഘടനയാണ് മനുഷ്യഭാഷയെ വ്യതിരിക്തമാക്കിയതും ഏറെ മുന്നോട്ടു നയിച്ചതും. ഉപകരണങ്ങളുടെ
ഉപയോഗത്തിനായി നമ്മുടെ പൂര്‍വികരുടെ മസ്തിഷ്കത്തിലുണ്ടായിരുന്ന നാഡീസംവിധാനങ്ങളില്‍ നിന്ന്
പരിണമിച്ചായിരിക്കാം വാക്യഘടനയുടെ ശ്രേണീവ്യവസ്ഥ രൂപപ്പെട്ടത് എന്ന് കരുതാം. ഒരു ഉപകരണം
രൂപമാറ്റം വരുത്തി മെച്ചപ്പെട്ട രീതിയില്‍ ഉപയോഗപ്പെടുത്തുന്ന രീതി ഭാഷോല്പത്തിയുടെ
കാര്യത്തിലും സഹായകമായിട്ടുണ്ടാവാം. ഉപകരണനിര്‍മാണത്തിലെ ഉപസംയോജനത്തെ രാമചന്ദ്രന്‍
ഉദാഹരിക്കുന്നു. അതുവരെ കൈകൊണ്ട് ഉപയോഗിച്ചിരുന്ന ഒരു കോടാലിത്തല മരത്തിന്റെ ഒരു പിടിയില്‍
ഉറപ്പിച്ച് പുതിയരീതിയില്‍ ഉപയോഗിക്കുന്നതിനെ നമുക്ക് ഉപസംയോജനമെന്ന് പറയാം. അല്ലെങ്കില്‍,
മറ്റൊരുദാഹരണം പറയാം. ‍മരങ്ങളുടെ മുകളിലുള്ള പഴങ്ങള്‍ പറിച്ചെടുക്കാന്‍ പൂര്‍വികര്‍ തോട്ടി
കെട്ടിയുണ്ടാക്കിയ രംഗം സങ്കല്പിക്കൂ. നീളം പോരെന്ന് തോന്നിയപ്പോള്‍ ഒരു കമ്പുകൂടെ ചേര്‍ത്ത്
അതിന്റെ നീളം വര്‍ധിപ്പിച്ചിരിക്കാം. ഇതേപോലെയായിരിക്കാം അവര്‍ വാക്കുകള്‍ തമ്മില്‍
ബന്ധിപ്പിക്കാന്‍ തുടങ്ങിയതും. ഇവിടെ ബാഹ്യസാമ്യമല്ല പ്രധാനം. ഉപകരണങ്ങള്‍ പടിപടിയായി
മെച്ചപ്പെടുത്തിയ ഉപസംയോജനവിദ്യയ്ക്ക് പ്രയോജനപ്പെട്ട മസ്തിഷ്കസംവിധാനങ്ങള്‍ തികച്ചും പുതിയൊരു
കാര്യത്തിനുവേണ്ടി- പദവിന്യാസ വ്യവസ്ഥ- പ്രയോജനപ്പെടുത്താന്‍ കഴിഞ്ഞു എന്നതിനാണ്
പ്രാധാന്യം.</p>
<h3 class="section-head" id="ചിന്തയുംഭാഷയും"><a href="#ചിന്തയുംഭാഷയും">ചിന്തയും ഭാഷയും</a></h3>
<p>മറ്റൊരു പ്രധാനപ്രശ്നം ഭാഷയും ക്രമനിബദ്ധ ചിന്തയുമായുള്ള ബന്ധമാണ്. ചിന്ത എന്നാലെന്താണ്
എന്നത് ആദ്യം വിശദീകരിക്കേണ്ടതുണ്ട്. ചില പ്രത്യേക നിയമങ്ങള്‍ക്കനുസരിച്ച് തുറന്ന സംജ്ഞകളെ
മസ്തിഷ്കം കൈകാര്യം ചെയ്യുന്നതിനെ ചിന്തയെന്ന് പറയാം. ചരടിന്റെ വലിവിനെക്കുറിക്കുന്ന ഹൂക്സ്
നിയമം (Hooke's law) ഊര്‍ജതന്ത്ര ക്ലാസിലിരുന്ന് പഠിച്ചിട്ടല്ല ഒരു ചിലന്തി വല നെയ്യുന്നത്.
പക്ഷേ, ഈ നിയമമനുസരിച്ചുള്ള വല നെയ്യാന്‍ അതിനറിയാം. ഇതില്‍ നിന്ന് വ്യത്യസ്തമായി ഹൂക്സ്
നിയമം പഠിച്ച എഞ്ചിനീയര്‍, നിരവധി തരത്തില്‍ അത് പ്രയോജനപ്പെടുത്താന്‍ കഴിയുംവിധം തന്റെ
മനസ്സില്‍ കൂടുതല്‍ തുറന്നതും വഴക്കമുള്ളതുമായ ചിത്രത്തോടുകൂടിയാണ് പ്രവൃത്തിയിലേക്ക് കടക്കുന്നത്.
ഇതിനെയാണല്ലോ ധാരണ എന്ന് പറയുന്നത്. ചിന്തയ്ക്ക് ഭാഷ ആവശ്യമാണോ? എല്ലാ കാലത്തേക്കും
തത്വചിന്തകര്‍ക്കായി മാറ്റിവെക്കാവുന്ന ഒരു വിഷയമാണിതെന്ന് പറയാം. എങ്കിലും വി എസ്
രാമചന്ദ്രന്റെ അഭിപ്രായത്തില്‍ ഭാഷയും ചിന്തയും വ്യതിരിക്തമായ കാര്യങ്ങളാണ്.</p>
<p>ചിന്തയും ഭാഷയുമായുള്ള ബന്ധത്തെ ഗൗരവമായി പരിശോധിച്ച മറ്റൊ രാള്‍ മനശ്ശാസ്ത്രജ്ഞനും
വിദ്യാഭ്യാസചിന്തകനുമായ വിഗോട്സ്കിയാണ്. മനുഷ്യരുടെ ഉയര്‍ന്ന മാനസികശേഷികളില്‍ ഏറ്റവും
പ്രാധാന്യം ഭാഷയ്ക്കാണ് എന്ന നിലപാടുകാരനായിരുന്നു അദ്ദേഹം. ചിന്തയേയും ഭാഷയേയും
വേര്‍തിരിച്ചു പഠിക്കുന്നത് ശരിയായ കാര്യമല്ലെന്ന് അദ്ദേഹം അഭിപ്രായപ്പെട്ടു. വെള്ളത്തെക്കുറിച്ചു
പഠിക്കേണ്ട ഒരാള്‍ അതിനായി ഹൈഡ്രജനെയും ഓക്സിജനെയും കുറിച്ചു പഠിക്കുന്നതുപോലെയാണത്. അങ്ങനെ
വേര്‍തിരിച്ചു പഠിക്കുന്നത് തെറ്റായ നിഗമനങ്ങളിലേക്ക് നമ്മെ നയിക്കും.</p>
<p>രണ്ടുവയസ്സിനു മുമ്പുള്ള ശിശുക്കളില്‍ ഭാഷാരഹിതചിന്തയും ചിന്താരഹിതഭാഷയുമുണ്ട്. രണ്ടു
വയസ്സിനുശേഷം വേറിട്ടു വികസിച്ച ഭാഷയും ചിന്തയും തമ്മില്‍ സമന്വയിച്ച് ഒരു പുതിയ
രൂപമുണ്ടാകുന്നതായി വിഗോട്സ്കി ചൂണ്ടിക്കാട്ടി. അപ്പോഴേക്കും കുട്ടിയുടെ ഭാഷ വാചികവും
യുക്തിപരവുമാകുന്നത് അതുകൊണ്ടാണ്. ചിന്തയും ഭാഷയും സമന്വയിക്കുന്ന ഈ ഘട്ടത്തോടെയാണ് കുട്ടിയുടെ
ഭാഷാപരമായ വളര്‍ച്ചയില്‍ കുതിപ്പുണ്ടാകുന്നത്. അതായത് ചിന്തയും ഭാഷയും വികാസം പ്രാപിക്കുന്നത്
വേറിട്ടാണെങ്കിലും അവ തമ്മില്‍ ഇഴപിരിച്ചെടുക്കാന്‍ കഴിയില്ല. ചിന്തയുടെ വികാസത്തിനുള്ള
ഉപകരണമായി ഭാഷ രൂപാന്തരപ്പെടുന്നു.</p>

<h3 class="section-head" id="സമൂഹമെന്നഘടകം"><a href="#സമൂഹമെന്നഘടകം">സമൂഹമെന്ന ഘടകം</a></h3>
<p>ശബ്ദാവയവങ്ങളുടെ പരിണാമത്തെക്കുറിച്ചും മസ്തിഷ്കത്തെക്കുറിച്ചുമെല്ലാമാണ് ഇത്രയും നേരം നാം
പരിശോധിച്ചത്. മനുഷ്യപരിണാമത്തില്‍ ശബ്ദാവയവങ്ങള്‍ക്കും മസ്തിഷ്കത്തിനുമുണ്ടായ വളര്‍ച്ച ഭാഷയുടെ
ആവിര്‍ഭാവത്തില്‍ ഒരു ഘടകം മാത്രമേ ആവുന്നുള്ളൂ. ഏറ്റവും പ്രധാനപ്പെട്ടത് സമൂഹം എന്ന ഘടകമാണ്.
സമൂഹം ഇല്ലെങ്കില്‍ ഭാഷയ്ക്ക് വലിയ പ്രസക്തിയില്ലല്ലോ. ഒറ്റയായ വ്യക്തിക്ക് ഭാഷയുടെ ആവശ്യമില്ല.
കൈകളുടെയും സംസാരേന്ദ്രിയങ്ങളുടെയും മസ്തിഷ്കത്തിന്റെയും ഈ ഒത്തുചേരല്‍ വ്യക്തിക്കുമാത്രമല്ല ഗുണം
ചെയ്തത് എന്നതും സുപ്രധാനമാണ്. അതിന്റെ നേട്ടങ്ങള്‍ സമൂഹത്തെയൊന്നാകെ മുന്നോട്ട് നയിച്ചു.</p>
<p>ഓരോ തലമുറയുടെയും നേട്ടങ്ങള്‍ അടുത്ത തലമുറയിലേക്ക് പകരുന്നതില്‍ ഭാഷ ശക്തമായ പിന്തുണയായി
മാറി. ഭാഷകൊണ്ടുള്ള നേട്ടം ഇതില്‍ മാത്രം ഒതുങ്ങുന്നില്ല. അമൂര്‍ത്തമായ ആശയങ്ങളുടെ
രൂപീകരണത്തിലേക്ക് ഭാഷ നമ്മെ നയിച്ചു. കെ വേണു അതിനെ പ്രപഞ്ചവും മനുഷ്യനും എന്ന പുസ്തകത്തില്‍
ഇങ്ങനെ വിശദീകരിക്കുന്നു.</p>
<p>"ഓരോ സമൂഹവും സമാഹരിക്കുന്ന അനുഭവസമ്പത്തും വിജ്ഞാനവും അടുത്ത തലമുറയിലേക്ക്
പകർത്തുന്നതിനായി ഉപയോഗിക്കപ്പെടുന്ന മാധ്യമമാണു് ഭാഷ. പക്ഷേ, നിഷ്ക്രിയമായ ഒരു മാധ്യമമായി
മാത്രമല്ല ഭാഷ വർത്തിക്കുന്നത്. അതിനു ക്രിയാത്മകമായ മറ്റൊരു വശം കൂടിയുണ്ട്. സമൂഹത്തിലെ
അനുഭവസമ്പത്ത് മുഴുവൻ ക്രോഡീകരിച്ചുവെക്കുന്നതോടെ ഭാഷയുടെ കർത്തവ്യം അവസാനിക്കുന്നില്ല. അങ്ങനെ
സമാഹരിക്കപ്പെട്ട അനുഭവങ്ങൾ തമ്മിൽ പുതിയ പുതിയ ബന്ധങ്ങളുണ്ടാക്കാൻ അതു സഹായിക്കുന്നു. ഓരോ
അനുഭവവും ഓരോ പദമോ പദസമൂഹമോ വഴിയാണേല്ലാ മസ്തിഷ്കത്തിൽ മുദ്രണം ചെയ്യപ്പെടുന്നത്.
ഇങ്ങനെയുള്ള പദങ്ങൾ തമ്മിൽ പുതിയ ബന്ധങ്ങൾ സ്ഥാപിക്കുന്നതുവഴി അവ പുതിയ ആശയങ്ങളായി മാറുന്നു.
വാസ്തവത്തിൽ ഓരോ പദവും ഓരോ വസ്തുവെയോ സംഭവത്തെയോ അതേപടി പ്രതിനിധീകരിക്കുന്നില്ല. ഒരു
പദത്തിന്റെ സാമൂഹ്യമായി അംഗീകരിക്കപ്പെട്ട അര്‍ത്ഥം പലപ്പോഴും ‘അമൂർത്ത’ മായിരിക്കും. ഒരു
പദം ഒരു പ്രത്യേക വസ്തുവിനെയല്ലാതെ ഒരു വര്‍ഗം വസ്തുക്കളുടെ പൊതുവായ ഗുണത്തെ
പ്രതിനിധീകരിക്കാറുണ്ട്. ‘മനുഷ്യൻ’ എന്ന പദം തന്നെയെടുക്കുക. ആ പദം ഏതെങ്കിലുമൊരു പ്രത്യേക
വസ്തുവിനെ പ്രതിനിധീകരിക്കുന്നില്ല. അതേസമയം ഒരു പ്രത്യേക വർഗം വസ്തുക്കളെ ഒരുമിച്ച്
പ്രതിനിധീകരിക്കുകയും ചെയ്യുന്നു. ഇങ്ങനെ വസ്തുക്കളുമായി നേരിട്ടു ബന്ധമില്ലാതെ തന്നെ,
അവയെക്കുറിച്ചുള്ള അമൂർത്താശയങ്ങൾ രൂപീകരിക്കാൻ ഭാഷയുടെ സഹായത്തോടെ നമുക്കു കഴിയുന്നു.
മനുഷ്യനൊഴിച്ചുള്ള ഒരു ജന്തുവിനും ഈ അമൂർത്തവൽക്കരണം സാധ്യമല്ല."</p>
<p>നേരിട്ട് അനുഭവവേദ്യമല്ലാത്ത അനുഭവങ്ങളെയും വസ്തുക്കളെയും പ്രതിനിധീകരിക്കുന്ന ആശയങ്ങള്‍
ഭാഷയുടെ സഹായത്തോടെ മനുഷ്യര്‍ സൃഷ്ടിക്കുന്നുണ്ട്. പുതിയ ആശയങ്ങള്‍ രൂപപ്പെടുന്നത് വ്യക്തികളുടെ
തലയ്ക്കകത്താണെങ്കിലും അത് മറ്റുള്ളവരിലേക്ക് പകരുന്നില്ലെങ്കില്‍ അവിടെവച്ച് തന്നെ നാശമടയുകയേ
ഉള്ളൂ. ആവിഷ്കര്‍ത്താവിനപ്പുറം കടക്കുന്ന ആശയങ്ങളാണ് ചരിത്രത്തിന്റെ ഭാഗമാവുന്നത്. ആശയങ്ങള്‍
പ്രകടിപ്പിക്കപ്പെട്ടിട്ടില്ലെങ്കില്‍ അത് ജനിച്ചിട്ടില്ല എന്നു പറയുന്നതാവും ശരി. അതായത്
ആശയങ്ങള്‍ ജീവിക്കുന്നത് ഒറ്റപ്പെട്ട തലയ്ക്കകത്തല്ല, മറിച്ച് ഒരുകൂട്ടം തലകള്‍ക്കകത്താണ്.
പരസ്പരവിനിമയം നടത്തുന്ന ഒരു സമൂഹത്തിന്റെ തലകള്‍ക്കകത്ത്.</p>
<p>വിശേഷമായ അറിവുകളും അതു കയ്യാളുന്ന ജനങ്ങളും എല്ലാ സമൂഹങ്ങളിലുമുണ്ടാവും. അവരുടെ
പാരസ്പര്യമാണ് സാമൂഹികമായ അറിവിനെ ഉല്പാദിപ്പിക്കുന്നത്. സാമൂഹിക പരിണാമത്തിന്റെ വ്യത്യസ്ത
ഘട്ടങ്ങളില്‍ വ്യത്യസ്തങ്ങളായ ആശയങ്ങള്‍ ഉടലെടുക്കുന്നുണ്ട്. ഓരോ കാലഘട്ടത്തിന്റെയും സവിശേഷതകളായ
ആശയസംഹിതകളെ ആ കാലഘട്ടത്തിന്റെ പ്രത്യയശാസ്ത്രങ്ങളെന്നു പറയാം. സൂക്ഷ്മമായി
പരിശോധിക്കുമ്പോള്‍ വ്യക്തികളുടെ ചിന്തകളും ആശയങ്ങളുമെല്ലാം നിലനില്‍ക്കുന്ന സാമൂഹിക
വ്യവസ്ഥിതിയുടെ ഉല്പന്നങ്ങളാണെന്നും കാണാം. ഓരോ കാലഘട്ടത്തിലെയും അധീശജ്ഞാനരൂപങ്ങളും
നിയമങ്ങളും കല്പനകളും മൂല്യങ്ങളുമെല്ലാം നിലനിര്‍ത്തപ്പെടുന്നതും അറിവിന്റെ നിയന്ത്രിതമായ
വിതരണത്തിലൂടെയാണ്.</p>
<h3 class="section-head" id="സാംസ്കാരികമുന്നേറ്റം"><a href="#സാംസ്കാരികമുന്നേറ്റം">സാംസ്കാരിക മുന്നേറ്റം</a></h3>
<p>നാച്വര്‍ മാസികയില്‍ (നവംബര്‍ 2008) വന്ന, ഭാഷ: വാക്കുകളുടെ ഒരു സാമൂഹിക ചരിത്രം
(Language: a social history of words) എന്ന ലേഖനത്തില്‍, ഉപകരണങ്ങള്‍ ഉണ്ടാക്കാനുള്ള
കഴിവ്, പഠിക്കാനുള്ള താല്പര്യം തുടങ്ങി മനുഷ്യരുടെ സവിശേഷതകളായി നാം കണക്കാക്കുന്ന നിരവധി
കാര്യങ്ങളുമായി പൊരുത്തപ്പെട്ടുകൊണ്ടാണ് ഭാഷയുടെ പരിണാമം ഉണ്ടായതെന്ന് ചൂണ്ടിക്കാണിക്കുന്നു.
മനുഷ്യരുടെ ഒരു പ്രത്യേക കഴിവിനെ മാത്രം എടുത്ത് പരിശോധിക്കുന്നത് ശരിയായിരിക്കില്ല.
മാത്രവുമല്ല, മനുഷ്യമസ്തിഷ്കം പ്രത്യേക കഴിവുകള്‍ക്കുവേണ്ടി സമര്‍പ്പിക്കപ്പെട്ട നിരവധി
മോഡ്യൂളുകളുടെ സമാഹാരമല്ല. സങ്കീര്‍ണമായ രീതിയില്‍ പല ഭാഗങ്ങള്‍ ബന്ധപ്പെട്ടു കിടക്കുന്ന ഘടനയാണ്
മനുഷ്യമസ്തിഷ്കത്തിനുള്ളത്. പല ഭാഗങ്ങളും ഒന്നിലേറെ പ്രവര്‍ത്തനങ്ങള്‍ക്ക് സജ്ജമാണ്. അതിനാല്‍
മനുഷ്യര്‍ക്ക് പരസ്പരബന്ധിതവും സങ്കീര്‍ണവുമായ ജ്ഞാനരൂപകല്പനകള്‍ ഉണ്ടാവാന്‍ സാധ്യതയുണ്ട്.</p>
<p>ഏതാണ്ട് 1,20,000 കൊല്ലം മുമ്പുതന്നെ വലിയതോതിലുള്ള ആശയവിനിമയത്തിന് ഭാഷ
മനുഷ്യപൂര്‍വികരുടെ സഹായത്തിനുണ്ടായിട്ടുണ്ടാവും എന്നാണ് കണക്കാക്കേണ്ടത്. അപ്പോഴേക്കും വലിയതോതില്‍
വേട്ട നടത്താന്‍ തുടങ്ങിയിരുന്നുവല്ലോ. കൂട്ടായ ഈ വേട്ടയാടല്‍ ഭാഷാശേഷിയെ പരിപോഷിപ്പിച്ച
ഘടകമാണ്. പ്ലിസ്റ്റോസീന്‍ കാലഘട്ടത്തിന്റെ (ഭൂമി രൂപംകൊണ്ടതിനുശേഷമുള്ള കാലത്തെ
പ്രീ-കാംബ്രിയൻ കാലഘട്ടം, പാലിയോസോയിക് യുഗം, സെനോസോയിക് യുഗം എന്നിങ്ങനെ
വേർതിരിക്കുന്നു. 6.5 കോടി വർഷങ്ങൾക്കു മുൻപ് തുടങ്ങി, ഇപ്പോഴും തുടരുന്നത് ഭൂമിയുടെ പ്രായത്തിലെ
സെനോസോയിക് യുഗമാണ്. പാലിയോസീൻ, ഇയോസീൻ, ഒളിഗോസീൻ, മയോസീൻ, പ്ലീയോസീൻ,
പ്ലിസ്റ്റോസീൻ, ഹോളോസീൻ എന്നിങ്ങനെ സെനോസോയിക് യുഗത്തെ വിവിധ കാലഘട്ടങ്ങളായി വീണ്ടും
വിഭജിക്കുന്നു.) അവസാനമാകുമ്പോഴേക്കും മീന്‍പിടിക്കല്‍, വര്‍ണങ്ങളുടെ ഉപയോഗം, ആയുധങ്ങളുടെ
നിര്‍മാണം തുടങ്ങി ബഹുമുഖമായ പ്രവര്‍ത്തനങ്ങളില്‍ മനുഷ്യര്‍ ഏര്‍പ്പെട്ടു തുടങ്ങിയിരുന്നു. ഭാഷാഉപയോഗവും
ഇതിനോടൊപ്പം അവര്‍ നേടിയെടുത്തിട്ടുണ്ടാവും എന്നാണ് കണക്കാക്കുന്നത്. തീര്‍ച്ചയായും ഇത്തരം
പ്രവര്‍ത്തനങ്ങളൊക്കെ ഗ്രൂപ്പുകളില്‍ നിന്ന് ഗ്രൂപ്പുകളിലേക്ക് പകരാന്‍ ഭാഷ ഉപകരിച്ചിരിക്കും. അതായത്
കൂട്ടായ്മയുടെയും സഹകരണത്തിന്റെതുമായ ഒരു അന്തരീക്ഷത്തിലാണ് ഭാഷ വളര്‍ന്നുവന്നത്. ഒരേ
വസ്തുവിലേക്ക് എല്ലാവരുടെയും ശ്രദ്ധ കേന്ദ്രീകരിക്കാനും ഒരേ ലക്ഷ്യത്തിനുവേണ്ടി ഒറ്റമനസ്സോടെ
പ്രവര്‍ത്തിക്കാനും ഭാഷ അവരെ സഹായിച്ചു. സംഘര്‍ഷങ്ങള്‍ പരിഹരിക്കാനും സഹകരണം ഉറപ്പുവരുത്താനും
വേട്ടയാടല്‍ സുഖകരമാക്കാനും അന്നത് അവരെ വളരെയേറെ സഹായിച്ചിട്ടുണ്ടാവും.</p>
<p>മറ്റ് ഏത് കഴിവിനെക്കാളും സാംസ്കാരിക പരിണാമത്തില്‍ മനുഷ്യവംശത്തെ മുന്നോട്ടുനയിച്ചത്
ഭാഷയായിരിക്കും. കാര്‍ഷികവൃത്തിയുടെ വലിയതോതിലുള്ള വൈവിധ്യവല്‍ക്കരണം ഏതാനും ആയിരം വര്‍ഷങ്ങള്‍
കൊണ്ട് സാധ്യമായത് ഭാഷയുടെ സഹായം കൊണ്ടുമാത്രമാണ്. വലിയ വലിയ കൂട്ടായ്മകളിലേക്ക്
കടക്കാനും സാംസ്കാരിക വിപ്ലവത്തിന് തുടക്കമിടാനും ഭാഷ ഉപകരിച്ചു. വിശ്വാസസംഹിതകള്‍,
നിയമങ്ങള്‍, സിദ്ധാന്തങ്ങള്‍ എന്നിവയെല്ലാം സാധ്യമായതും മറ്റൊന്നിനാലല്ല. പുതുതായി രൂപം
നല്‍കാന്‍ കഴിഞ്ഞ സാമൂഹികവും ഭൗതികവുമായ ഈ ലോകമാണ് മനുഷ്യപരിണാമത്തെപ്പോലും മുന്നോട്ടു
നയിച്ചത്. ആയിരം ജീനുകളെക്കാളും ശ്രേഷ്ഠമാണ് ഒരു വാക്ക് എന്നു പറഞ്ഞാല്‍ അത് വീണ്‍വാക്കാവില്ല
തന്നെ. കാരണം, ഏര്‍സ് സ്വസ്‌ത്‌മാരി (Eörs Szathmáry) യും സബോഷ് സമാനോ (Szabolcs
Számadó) യും ചൂണ്ടിക്കാണിച്ചതുപോലെ ഭാഷയാണ് പരിണാമസംബന്ധിയായ ഏറ്റവും പ്രധാന
കണ്ടുപിടിത്തം. അത് വൈജ്ഞാനിക മുന്നുപാധികളിന്മേല്‍ നിര്‍മിച്ചതും മറ്റെല്ലാറ്റിലേക്കും വാതില്‍
തുറക്കുന്നതുമാണ്.</p>
<nav class="pagination pager flat">
    <ul>
        <li class="prev">
            <span class="label tag">Prev</span>
            <a href="../2_samoohavum_arivum">സമൂഹവും അറിവും</a>
        </li>
        <li class="next">
            <span class="label tag">Next</span>
            <a href="../4_sasthramenna_anweshanareethi">ശാസ്ത്രം എന്ന അറിവന്വേഷണരീതി</a>
        </li>
    </ul>
</nav>
{{< /rawhtml >}}
